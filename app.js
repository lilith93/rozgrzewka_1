function sortFunction(prev, next){
    return prev.toString().localeCompare(next, undefined, { numeric: true });
}

module.exports = array => {
    return array.sort(sortFunction);
}