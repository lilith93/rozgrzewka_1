const classUnderTests = require('../app');
const expect = require('chai').expect;

describe('Sort Function', function(){
    it('should sort array first by numbers then by strings', function(){
        // Arrange
        var arrayToSort = [6, 'a', 1, 'asd', 32];
        var expectedArray = [1, 6, 32, 'a', 'asd'];
        
        // Act
        classUnderTests(arrayToSort);

        // Assert
        expect(arrayToSort).to.be.eql(expectedArray);
    }),

    it('should sort properly array containing only letters and words', function(){
        // Arrange 
        var arrayToSort = ['zorro', 'r', 'array', 'p', 'o'];
        var expectedArray = ['array', 'o', 'p', 'r', 'zorro'];

        // Act
        classUnderTests(arrayToSort);

        // Assert
        expect(arrayToSort).to.be.eql(expectedArray);
    }),

    it('should sort properly array containing only numbers', function(){
        // Arrange 
        var arrayToSort = [100, 1, 10, 32, 1000, 123];
        var expectedArray = [1, 10, 32, 100, 123, 1000];

        // Act
        classUnderTests(arrayToSort);

        // Assert
        expect(arrayToSort).to.be.eql(expectedArray);
    })
});